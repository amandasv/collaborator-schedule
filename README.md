#Agenda do Colaborador

O objetivo do presente projeto é facilitar a organização dos compromissos diários dos colaboradores.

##Próximos passos:

- Possibilidade de cadastro de compromissos;
- Possibilidade de edição de compromissos;
- Possibilidade de visualização de compromissos;
- Possibilidade de exclusão de compromissos;

- Implementar a agenda em forma de calendário;
- Alerta caso o usuário queira cadastrar mais de um evento para o mesmo dia e horário;
- Criação de login para o colaborador, assim seus compromissos ficam visíveis somente a ele;

- Implementação de tasks para compilação e minificação de scss

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/class/module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).