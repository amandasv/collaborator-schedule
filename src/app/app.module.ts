import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { routing } from './app.routing';

import { MaterializeModule } from 'angular2-materialize';

import { AppComponent } from './app.component';
import { SheduleComponent } from './shedule/shedule.component';
import { EventsComponent } from './shedule/events/events.component';

import { EventsService } from './shedule/events/events.service';
import { HomeComponent } from './home/home.component';
import { EventFormComponent } from './shedule/events/event-form/event-form.component';
import { NavbarComponent } from './navbar/navbar.component';

@NgModule({
  declarations: [
    AppComponent,
    SheduleComponent,
    EventsComponent,
    HomeComponent,
    EventFormComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MaterializeModule,
    routing
  ],
  providers: [EventsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
