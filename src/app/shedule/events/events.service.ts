import { Injectable } from '@angular/core';

@Injectable()
export class EventsService {

  private events: any = [
    {
      'title': 'Primeiro Evento',
      'start': new Date(2017, 5, 23),
      'end': new Date(2017, 5, 25),
      'description': 'Descrição do Evento',
      'id': 123,
    },
    {
      'title': 'Segundo Evento',
      'start': new Date(2017, 6, 25),
      'end': new Date(2016, 6, 27),
      'description': 'Descrição segundo evento',
      'id': 125,
    },
  ];

  constructor() { }

  getEvents() {
    return this.events;
  }

}
