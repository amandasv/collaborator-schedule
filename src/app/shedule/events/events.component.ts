import { Component, OnInit } from '@angular/core';

import { EventsService } from './events.service';

import { Event } from './event';


@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {

  private events: Event[] = [];

  constructor(private eventsService: EventsService) {}

  ngOnInit() {
    this.events = this.eventsService.getEvents();
  }

}
