import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { EventFormComponent } from './shedule/events/event-form/event-form.component';

const APP_ROUTES: Routes = [
  { path: '' , component: HomeComponent },
  { path: 'criar' , component: EventFormComponent },

];

export const routing: ModuleWithProviders = RouterModule.forRoot(APP_ROUTES);
