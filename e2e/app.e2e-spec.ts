import { CollaboratorShedulePage } from './app.po';

describe('collaborator-shedule App', () => {
  let page: CollaboratorShedulePage;

  beforeEach(() => {
    page = new CollaboratorShedulePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
